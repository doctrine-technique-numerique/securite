# 3. Mise en application : les textes de référence

La démarche de sécurisation des systèmes d’information comme toutes les procédures de sécurité numérique s’adossent à des textes législatifs ou réglementaires les encadrant. 

Pour rappel, sont listés ci-dessous les textes suivant les différents périmètres, sachant que ces corpus convergent vers les attentes de la doctrine rappelées plus haut.

### Cadre réglementaire général s’appliquant aux services et infrastructures numériques opérés par le ministère, ses services déconcentrés et les établissements publics relevant de la tutelle ministérielle et des prestataires y intervenant

#### Textes relatifs au numérique, à la sécurité du numérique et des systèmes d’information et de communication s’appliquant aux ministères, services déconcentrés et établissements publics relevant de la tutelle du ministère

* Décret n° 2022-513 du 8 avril 2022 relatif à la sécurité numérique du système d'information et de communication de l'État et de ses établissements publics (NOR : PRMD2135717D). 
* Arrêté du 26 octobre 2022 portant approbation de l’instruction générale interministérielle n° 1337/SGDSN/ANSSI sur l’organisation de la sécurité numérique du système d’information et de communication de l’État et de ses établissements publics (NOR :  PRMD2221955A).
* Décret n° 2019-1088 du 25 octobre 2019 relatif au système d’information et de communication de l’État et à la direction interministérielle du numérique (NOR : PRMG1929496D).
* Le référentiel général de sécurité pris en application du décret n° 2010-112 du 2 février 2010 pris pour l'application des articles 9, 10 et 12 de l'ordonnance n° 2005-1516 du 8 décembre 2005 relative aux échanges électroniques entre les usagers et les autorités administratives et entre les autorités administratives (NOR : PRMX0909445D). 
* Arrêté du 13 juin 2014 portant approbation du référentiel général de sécurité et précisant les modalités de mise en œuvre de la procédure de validation des certificats électroniques (NOR : PRMD1413745A). 
* Circulaire du Premier ministre n° 5725/SG du 17 juillet 2014 introduisant la politique de sécurité des systèmes d’information de l’État (PSSIE) (NOR : PRMX1420095C). 
* Instruction interministérielle n° 901/SGDSN/ANSSI relative à la protection des systèmes d’information sensibles (NOR : PRMD1503279J). 
* Ordonnance n° 2005-1516 du 8 décembre 2005 relative aux échanges électroniques entre les usagers et les autorités administratives et entre les autorités administratives (NOR : ECOX0500286R). 
* Ordonnance n° 2017-1426 relative à l'identification électronique et aux services de confiance pour les transactions électroniques (NOR : PRMD1724021R). 

#### Textes relatifs à la protection des données personnelles

* Loi n° 2018-493 du 20 juin 2018 relative à la protection des données personnelles (NOR : JUSC1732261L).
* Décret n° 2019-536 du 29 mai 2019 pris pour l'application de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, notamment son article 85 et ses articles 140 et suivants (NOR : JUSC1911425D). 

#### Textes concernant des périmètres spécifiques

* Arrêté du 13 juillet 2020 fixant les règles de sécurité et les modalités de déclaration des systèmes d'information d'importance vitale et des incidents de sécurité relatives au sous-secteur d'activités d'importance vitale « Recherche publique » (NOR : PRMD2018060A) et leurs annexes. 
* Décret n° 2018-384 du 23 mai 2018 relatif à la sécurité des réseaux et systèmes d’information de services essentiels (NOR : PRMD1809740D).
* Arrêté du 14 septembre 2018 fixant les règles de sécurité et les délais mentionnés à l'article 10 du décret n° 2018-384 du 23 mai 2018 relatif à la sécurité des réseaux et systèmes d'information des opérateurs de services essentiels et des fournisseurs de service numérique (NOR : PRMD1824939A).
* Décret n° 2011-1425 du 2 novembre 2011 portant application de l'article 413-7 du code pénal et relatif à la protection du potentiel scientifique et technique de la nation (NOR : PRMX1118649D).
* Arrêté du 3 juillet 2012 relatif à la protection du potentiel scientifique et technique de la nation (NOR : PRMX1227979A). 
* Circulaire interministérielle n° 3415/SGDSN/AIST/PST du 7 novembre 2012 de mise en œuvre du dispositif de protection du potentiel scientifique et technique de la nation (NOR: PR1vID1238889C). 

#### Texte relatif à la protection du secret de la défense nationale

* Arrêté du 9 août 2021 portant approbation de l’instruction générale interministérielle n° 1300 sur la protection du secret de la défense nationale (NOR : PRMD2123775A).

#### Texte relatif à l’organisation gouvernementale pour la gestion de crises majeures

* Circulaire n° 6095/SG du 1er juillet 2019 relative à l'organisation gouvernementale pour la gestion de crises majeures.

### Cadre réglementaire général s’appliquant aux services et infrastructures numériques opérés par les collectivités territoriales et des prestataires y intervenant

L’ANSSI a rappelé dans le document « [Sécurité numérique des collectivités territoriales](https://cyber.gouv.fr/sites/default/files/2020/01/anssi-guide-securite_numerique_collectivites_territoriales-reglementation1.pdf "Sécurité numérique des collectivités territoriales") » qui précise la nécessité de tenir des homologations ainsi que le respect du RGPD.

Ainsi, les textes relatifs au numérique, à la sécurité du numérique et des systèmes d’information et de communication s’appliquant aux services et infrastructures numériques opérés par les collectivités territoriales et les prestataires reprennent notamment les lois ou règlements suivants :

#### Textes relatifs au numérique, à la sécurité du numérique et des systèmes d’information et de communication

* Le référentiel général de sécurité pris en application du décret n° 2010-112 du 2 février 2010 pris pour l'application des articles 9, 10 et 12 de l'ordonnance n° 2005-1516 du 8 décembre 2005 relative aux échanges électroniques entre les usagers et les autorités administratives et entre les autorités administratives (NOR : PRMX0909445D). 
* Arrêté du 13 juin 2014 portant approbation du référentiel général de sécurité et précisant les modalités de mise en œuvre de la procédure de validation des certificats électroniques (NOR : PRMD1413745A). 
* Ordonnance n° 2005-1516 du 8 décembre 2005 relative aux échanges électroniques entre les usagers et les autorités administratives et entre les autorités administratives (NOR : ECOX0500286R). 
* Ordonnance n° 2017-1426 relative à l'identification électronique et aux services de confiance pour les transactions électroniques (NOR : PRMD1724021R).