# 1. Préambule

## 1.1. Contexte

Le présent document se place dans la démarche de la doctrine technique du numérique pour l’éducation dans son volet de sécurité numérique. Il s’agit de fournir à chaque acteur d’un dossier, selon ses prérogatives, un cadre général permettant d'opérer les actions requises afin de garantir la sécurité des services pour l’Éducation dans son ensemble, en lien avec les autres acteurs compétents et de manière cohérente avec l’ensemble de la doctrine.

D’une manière générale, chaque organisme – établissement, opérateur, direction des systèmes d’information – comme chaque type d’application, peuvent présenter des enjeux de sécurité qui lui sont spécifiques et en conséquence des mesures opérationnelles différentes. Néanmoins, la dématérialisation des procédures, l’interconnexion des différents systèmes d’information, la consommation des mêmes données par des systèmes hétérogènes, l’exposition sur Internet des télé-services, conjointement à la forte croissance des attaques informatiques, contribuent à faire de la sécurité numérique un enjeu global et partagé, qu’il est nécessaire de promouvoir de manière commune dans le contexte de la présente doctrine.

## 1.2. Objectifs du document

La sécurité numérique a pour objectif premier la mise en œuvre de mesures techniques, organisationnelles, procédurales pour assurer la protection des systèmes et des personnes. Les responsabilités des différents acteurs, qui partagent juridiquement et techniquement les compétences liées à la sécurité, sont à conjuguer avec subsidiarité.

En conséquence, la comitologie qu’il convient de mettre en place pour assurer le partage d’information est un axe essentiel à développer pour chaque périmètre au début de la démarche de conception des systèmes, permettant de mettre en lien les équipes locales et les équipes transversales.

D’un point de vue légal et réglementaire, les corpus de règles peuvent être communs à tous les acteurs (RGPD, EIDAS[^1], SREN[^2], codes spécifiques, etc.) ou différer sur la forme mais convergent sur le fond. Dès lors, au-delà de la lettre, c’est l’esprit de la doctrine, selon laquelle les efforts conjoints visent une sécurité globale, qu’il convient de promouvoir, chacun selon son périmètre et selon l’aspect réglementaire qui s’y exerce.

Ainsi, la loi n° 2013-595 d'orientation et de programmation pour la refondation de l'École de la République du 8 juillet 2013 est commune à chaque périmètre et rappelle dans ses dispositions que la maintenance et l’équipement informatique des établissements publics locaux d’enseignement (EPLE) sont à la charge des collectivités territoriales, à l’exception des services numériques contractualisés directement par le chef d’établissement. Dès lors, les aspects d'architecture informatique pour le périmètre des établissements, mais également pour les services mutualisés ainsi que les aspects d’exploitation des différents systèmes d’information, relèvent de la responsabilité des collectivités territoriales.

Les domaines de partage de connaissances et d’échange permettant de mutualiser les compétences en termes de sécurité numérique relèvent de cinq registres :

* les homologations des systèmes d’information ou des applications,
* les contrôles de sécurité ou audits suivant les référentiels techniques,
* les clauses de sécurité dans les marchés,
* la gestion d’incidents de sécurité numérique,
* la politique opérationnelle de sécurité numérique commune.

## 1.3. Cycle de vie du document et gouvernance

Le ministère et ses services déconcentrés ainsi que les collectivités territoriales ont participé à l’élaboration de ce cadre général.

Le présent document constitue la **première version** du cadre général de sécurité des services numériques pour l’éducation.

Une mise à jour annuelle du document est prévue à l’instar de la doctrine technique du numérique pour l’éducation.

[^1] https://cyber.gouv.fr/le-reglement-eidas-n9102014
[^2] https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000049563368