# 2. Les règles de sécurité du numérique pour l’éducation composant la doctrine technique

## 2.1. Relativement aux homologations

Le ministère est tenu d’homologuer ses systèmes d’information et les services numériques qui y sont liés selon plusieurs référentiels et de fournir le corpus minimal pour permettre aux autorités d’engager les usagers à utiliser ces services sachant que la sécurité juste et adaptée a été évaluée par les comités compétents.

Plusieurs véhicules réglementaires le rappellent (RGS, PSSIE, instructions interministérielles ou ministérielles). Les autorités qualifiées pour la sécurité des systèmes d’information et les autorités d’homologation pour mener à bien ces chantiers sont nommés par voie réglementaire.

Les dossiers d’homologation intègrent l’aspect RGPD tant dans sa dimension d’inscription au registre que d’étude d’impact potentielle. Plus généralement, tous les acteurs impliqués dans la construction et l’exploitation des services numériques sont tenus par le RGPD.

À noter que le respect du RGPD emporte également – du fait des principes de minimisation des données manipulées qu’il met en exergue – les conditions de réduction de l'empreinte environnementale. Il paraît opportun de lier les deux concepts dans les travaux de manière à prendre en compte conjointement les attentes des deux domaines.

!!! regle "Règle"
    Les autorités qualifiées de la sécurité des systèmes d’information mettent en place les homologations des services numériques qu’elles ont en responsabilité et invitent les services de sécurité compétents des autorités partenaires pour des partages d’information relativement aux commissions d’homologation.

## 2.2. Relativement aux contrôles de sécurité ou audits suivant les référentiels techniques

Concernant le contrôle de l’application des référentiels de sécurité publiés par l’ANSSI ou de la CNIL, il convient que les audits techniques soient élaborés de manière partagée entre les parties prenantes notamment les services techniques des collectivités et ceux de la région académique en vue de la tenue d’une commission d’homologation où chacune des parties est représentée.

Ces audits réguliers sont à la fois versés au dossier des commissions d’homologation et sont également une opportunité pour améliorer conjointement le niveau de sécurité des systèmes d’information contribuant à l’offre de services numériques pour l’éducation.

!!! regle "Règle"
    Les audits techniques des services numériques pour l’éducation sont conçus et réalisés de manière conjointes par les équipes techniques académiques et celles des partenaires ainsi que des acteurs tiers intervenant dans l’offre de services. Ces équipes pourront mandater conjointement des audits externes par prestation le cas échéant. Un document socle précisant les points de contrôle et d’audit minimaux et commun à toutes les entités opérant sur les systèmes d’information offrant des services numériques pour l’éducation sera prescrit par un groupe de travail regroupant des différents acteurs du domaine.

## 2.3. Relativement aux clauses de sécurité dans les marchés

L’élaboration des services numériques pour l’éducation, lorsqu’il passe par des prestations mises en places suite à des marchés publics, doit prendre en compte les règles réglementaires relatives à la sécurité des systèmes d’information dès la rédaction du cahier des charges de manière à ce que l’exécution du marché puisse permettre aux équipes en charge de la mise en œuvre de commander les prestations adéquates. Dès lors, le partage d’information préalable concernant les clauses de sécurité des systèmes d’information dans les travaux de rédaction des cahiers des charges s’avère essentiel pour le bon déroulé du marché par la suite. 

Ces efforts conjoints permettent notamment de faire converger les bonnes pratiques en matière de sécurité des systèmes d’information édictées par voies légale ou réglementaire s’appliquant dans les différents périmètres d’action des acteurs des services numériques pour l’éducation.

!!! regle "Règle"
    Les clauses de sécurité dans les marchés font l’objet d’une élaboration conjointe entre les équipes techniques à l’œuvre pour la construction des services du numérique pour l’éducation de manière à disposer des conditions d’exploitation sécurisée dans toutes les prestations existantes.

## 2.4.	Relativement à la gestion d’incidents de sécurité numérique

Le traitement des incidents de niveau majeur relève d’un traitement conjoint entre les RSSI académiques, qui sont en charge du signalement des incidents de sécurité numérique auprès du centre opérationnel de la sécurité des systèmes d’information du ministère (Cossim), et les RSSI des collectivités territoriales.

Chaque chef d’établissement, sans être AQSSI, constitue le point de contact de la sécurité numérique pour les services déconcentrés de l’éducation nationale et particulièrement pour la chaîne d’alerte du périmètre dont il est usager ou dont il a la responsabilité, notamment en cas d’incident de sécurité du numérique qui affecte fortement les activités de son établissement.

Pour ce faire, chaque académie maintient un annuaire des points de contacts en EPLE.

Le RSSI académique informe et prend l’assistance du Cossim de manière à ce que l’information soit traitée dans le contexte global des incidents touchant le ministère et en lien avec les autorités décisionnelles du ministère, en lien également avec l’ANSSI et les forces de l’ordre si nécessaire. 

Il est essentiel que les échelons décisionnels du ministère soient en parfaite connaissance des incidents de sécurité survenant sur l’ensemble des services du numérique pour l’éducation dès leur survenance afin de pouvoir gérer les impacts de ceux-ci, diffuser l’information de nouvelles menaces à l’ensemble des acteurs des SI, qu’ils soient dans le périmètre lié aux ressources numériques pour l’éducation ou dans des périmètres connexes, y compris les équipes des collectivités territoriales pour leur propres besoins.

Il est primordial de faire cause commune dans le traitement et la gestion des incidents tant la rapidité de réaction vis-à-vis de nouveaux scénarios d’attaques est la clé pour limiter l’impact de ces attaques voire pour contrer totalement les tentatives grâce à la prévention ou la sensibilisation de l’ensemble des acteurs.

!!! regle "Règle"
    Les incidents de sécurité doivent faire l’objet de déclarations aux chaînes d’alerte adéquates selon les fiches de remontée d’incident diffusées à l’ensemble des acteurs des services numériques pour l’éducation.

## 2.5.	Relativement à une politique opérationnelle de sécurité numérique

La définition de politiques opérationnelles a été l’objet de nombreux travaux sectoriels (par domaines industriels, administratifs, métiers, spécialités) et a permis de clarifier pour tous les acteurs concernés ce qui est attendu et à l’état de l’art tant dans l’élaboration des architectures que dans l’exploitation des systèmes d’information ou que dans les développements d’applications.

Dans la gestion partagée des traitements de données dans les services du numérique pour l’éducation, les interconnexions des systèmes d’information par interfaçage des différents périmètres conduisent à traiter conjointement les évolutions en fonction des besoins, des spécifications, mais également des remédiations consécutivement aux incidents et aux alertes. Il convient donc de définir un cadre qui puisse guider tous les acteurs dans la construction des services numériques à venir ou dans la conduite de ceux existant.

Un tel cadre général sera revu régulièrement par une comitologie constituée des acteurs de la filière du numérique pour l’éducation et des acteurs de la sécurité des systèmes d’information et soumis à validation au comité des partenaires.

!!! regle "Règle"
    Les équipes techniques des domaines liés au numérique pour l’éducation établissent conjointement, en lien avec les équipes de la sécurité numérique du ministère et des collectivités territoriales, une politique opérationnelle de la sécurité numérique (POSN) définissant un corpus de règles techniques à respecter dans la construction, l’exploitation et l’organisation du service numérique pour l’éducation de manière sécurisée.

